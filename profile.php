<?php
	require_once("handler/database_handler.php");
	require_once("handler/user_handler.php");
	require_once("handler/db_video_handler.php");
	session_start();
	//$_SESSION['authenticated'] = 1;
	//$_SESSION['email'] = "reader@asds";
	
	if( isset($_SESSION['authenticated'])){
		if(isset($_GET['pid'])){
			$current_user = new user_handler;
			$current_user->retrieve_from_email($_SESSION['email']);
			$visiting_user = new user_handler;
			$visiting_user->retrieve_from_id(htmlspecialchars(trim($_GET['pid'])));
			if($visiting_user->is_valid()){
			
		?>
<!--
 .d8888b.  888                                                                    
d88P  Y88b 888                                                                    
Y88b.      888                                                                    
 "Y888b.   888888  8888b.  88888b.   .d8888b .d88b.                               
    "Y88b. 888        "88b 888 "88b d88P"   d8P  Y8b                              
      "888 888    .d888888 888  888 888     88888888                              
Y88b  d88P Y88b.  888  888 888  888 Y88b.   Y8b.                                  
 "Y8888P"   "Y888 "Y888888 888  888  "Y8888P "Y8888                               
                                                                                  
                                                                                  
                                                                                  
8888888                                              888    d8b                   
  888                                                888    Y8P                   
  888                                                888                          
  888   88888b.  88888b.   .d88b.  888  888  8888b.  888888 888  .d88b.  88888b.  
  888   888 "88b 888 "88b d88""88b 888  888     "88b 888    888 d88""88b 888 "88b 
  888   888  888 888  888 888  888 Y88  88P .d888888 888    888 888  888 888  888 
  888   888  888 888  888 Y88..88P  Y8bd8P  888  888 Y88b.  888 Y88..88P 888  888 
8888888 888  888 888  888  "Y88P"    Y88P   "Y888888  "Y888 888  "Y88P"  888  888 
                                                                                  
                                                                                  
                                                                                  
 .d8888b.                             888                                         
d88P  Y88b                            888                                         
888    888                            888                                         
888         .d88b.  88888b.   .d8888b 888  8888b.  888  888  .d88b.               
888        d88""88b 888 "88b d88P"    888     "88b 888  888 d8P  Y8b              
888    888 888  888 888  888 888      888 .d888888 Y88  88P 88888888              
Y88b  d88P Y88..88P 888  888 Y88b.    888 888  888  Y8bd8P  Y8b.                  
 "Y8888P"   "Y88P"  888  888  "Y8888P 888 "Y888888   Y88P    "Y8888               
                                                                                  
                                                                                  
                                                                                  
 .d8888b.   .d8888b.   d888  888888888                                            
d88P  Y88b d88P  Y88b d8888  888                                                  
       888 888    888   888  888                                                  
     .d88P 888    888   888  8888888b.                                            
 .od888P"  888    888   888       "Y88b                                           
d88P"      888    888   888         888                                           
888"       Y88b  d88P   888  Y88b  d88P                                           
888888888   "Y8888P"  8888888 "Y8888P"   
-->

		<!DOCTYPE HTML>

		<html>
			<head>
				<title><?php echo $visiting_user->fetch_name();?> || SIC - 2015</title>

				<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
				<script src="./js/jquery.min.js"></script>
				<script src="./js/bootstrap.min.js"></script>
				<!-- Custom Theme files -->
				<!--theme-style-->
				<link href="./css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />	

				<link href="./css/style.css" rel="stylesheet" type="text/css" media="all" />	
				<link href="./css/external-style.css" rel="stylesheet" type="text/css" media="all" />	
				
				<!--//theme-style-->
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="keywords" content="STANCE Innovation Conclave, Innovation Challenge, STANCE, SIC" />
				<script type="application/x-javascript">addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
				
				<!--Start of Live Chat Script-->
				<script type="text/javascript">
				window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
				d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
				_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
				$.src="//v2.zopim.com/?2u9qSbqgRjMPv1KwrsH5ChGkQZ6xrp0x";z.t=+new Date;$.
				type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
				</script>
				<!--End of Live Chat Script-->
				
				<script language="javascript">
				function ClipBoard()
					{
					holdtext.innerText = copytext.innerText;
					Copied = holdtext.createTextRange();
					Copied.execCommand("Copy");
					}
				</script>
				
				
			</head>

			<body>
				<!-- Start of Facebook Share Script -->
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<!-- End of Facebook Share Script -->
				
				<!--Start of Twitter Share Script-->
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<!--End of Twitter Share Script-->
				
				<header class="container-fluid">
					<div class="row">
						<div class="col-md-12 status-bar" style="height:70px;">
							<div class="col-md-2 col-md-offset-1 logo">
								<img src="images/log.png">
							</div>

							<div class="col-md-9">
								<div class="row">
									<div class="pull-right col-md-2 account-link">
										<div class="dropdown">
										  <button id="dLabel" type="button" class="account-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										    <?php echo $current_user->fetch_name();?>
										    <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel" >
											<a href="dashboard.php"><li><span style="padding-left:10px">Dashboard</span></li></a>
										    <li class="divider"></li>
										    <a href="#"><li><span style="padding-left:10px">Contact Us</span></li></a>
										    <li class="divider"></li>
										    <a href="#"><li><span style="padding-left:10px">Rules</span></li></a>
										    <li class="divider"></li>
										    <a href="logout.php"><li><span style="padding-left:10px">Sign Out</span></li></a>
										  </ul>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</header>
				<br>
				<section>
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							 <div class="btn-group " >
							  <a href="dashboard.php"><button type="button" class="col-md-4 btn btn-primary">Dashboard</button></a>
							  <a href="rankings.php"><button type="button" class="col-md-4 btn btn-primary">Rankings</button></a>
							  <a href="invitation.php"><button type="button" class="col-md-4 btn btn-success  ">Request Invite</button></a>
							</div>
						</div>
						</div>
						<br /><br />
					</div>
					<div class="row">
						<div class="col-md-3 left-sidebar">
							<ul class="list-group">
							  <li class="list-group-item">
							    <b><u>Submissions </u></b>
							  </li>
							  <?php
								$result = $visiting_user->retrieve_submissions_from_email($visiting_user->fetch_email());								
								
								if($result->num_rows > 0){
									 while($row = $result->fetch_assoc()) {        
										?>
										   <a href="./profile.php?pid=<?php echo $visiting_user->fetch_id();?>&vid=<?php echo $row['video_id'];?>">
											   <li class="list-group-item">
												<span class="badge"><?php echo $row['views'];?></span>
												<?php
												echo $row['title'];
												?>
											   </li>
										   </a>
										<?php
									}
								}
								else{
								?>
									<li class="list-group-item">
									<?php
										
									?>
									<span class="badge">0</span>
									No Submissions yet!
									</li>							   
								<?php	
								}
								
							  ?>
							   
							</ul>
						</div>
						<div class="col-md-8 main-section" style="height:500px;">
							<?php
								if(isset($_GET['vid'])){
									$current_video = new db_video_handler;
									$current_video->fetch_details_from_id(htmlspecialchars(trim($_GET['vid'])));
									
									if($current_video->is_valid() AND ($current_video->fetch_user_id() == htmlspecialchars(trim($_GET['pid'])))){
										
							?>
							<div class="col-md-12 panel-default">
								<br />
								<div class="row">
									<div class="panel-heading padding-left-30" ><h2><span class="alert alert-info"><?php echo $current_video->fetch_title();?></span></h2></div>
								</div>
								<div class="row display-panel-dashboard">
									<iframe width="560" height="315" src="<?php echo $current_video->fetch_embed_url();?>" frameborder="0" allowfullscreen></iframe>
								</div>
								<div class="row display-panel-dashboard">
									<div class="row">
										<span class="fb-share-button"  data-href="http://stanceinnovationconclave.com/profile.php?pid=<?php echo $current_video->fetch_user_id();?>&vid=<?php echo $current_video->fetch_id();?>" data-layout="button_count"></span>
										<span style="margin-left:200px;"><a href="https://twitter.com/share" class="twitter-share-button" data-text="I liked this at SIC-'15" data-via="our_stance" data-hashtags="InnovationVenerated">Tweet this video</a></span>
									</div>
									<br />
									<span>
										<a href="profile.php?pid=<?php echo $current_video->fetch_user_id();?>&vid=<?php echo $current_video->fetch_id();?>">This video</a> 
										was submitted by <b><a href="profile.php?pid=<?php echo $current_video->fetch_user_id();?>"><?php echo $current_video->fetch_user_name();?></a></b> 
										and has received <b><?php echo $current_video->fetch_views();?></b> recorded views till date. 
							
									</span>
								</div>
							</div>
							<?php
									}
									else{
							?>
							<div class="col-md-12 panel-default">
								<br />
								<div class="row">
									<div class="panel-heading padding-left-30" ><h2><span class="alert alert-danger">Error!</span></h2></div>
								</div>
								<div class="row display-panel-dashboard">
									Either this video does not belong to <a href="profile.php?pid=<?php echo $visiting_user->fetch_id();?>"><b><?php echo $visiting_user->fetch_name();?></b></a> or <b>it doesn't exist</b>.
									<br ><br >
									<a href="dashboard.php"><span class="btn btn-success">Go to dashboard</span></a>
								</div>
							</div>
							<?php
									}
								}
								else{
							?>
							<div class="col-md-12 panel-default">
								<br />
								<div class="row display-panel-dashboard">
									<div class="panel-heading padding-left-30" ><h2><span class="alert alert-info"><?php echo $visiting_user->fetch_name();?></span></h2></div>
								</div>
							</div>
							<?php
								}
							?>
						</div>
					</div>
				</section>
				<br><br>
				<div class="footer">
					<div class="container">
						
						<p class="footer-class">Copyright © 2015 <a href="http://www.stance.world/" target="_blank">STANCE</a> </p>
					</div>
				</div>
				
				<script src="js/user.js"></script>
				
				<!-- Piwik -->
				<script type="text/javascript">
				  var _paq = _paq || [];
				  _paq.push(['trackPageView']);
				  _paq.push(['enableLinkTracking']);
				  (function() {
					var u="//stanceinnovationconclave.com/piwik/";
					_paq.push(['setTrackerUrl', u+'piwik.php']);
					_paq.push(['setSiteId', 1]);
					var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
					g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
				  })();
				</script>
				<noscript><p><img src="//stanceinnovationconclave.com/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
				<!-- End Piwik Code -->


				
			</body>

		</html>

		

<?php
			}
			else{
				header("location:dashboard.php");
			}
		}
		
		else{
			header("location:dashboard.php");
		}
	}
	
	else{
		header("location:index.php");
	}

?>
