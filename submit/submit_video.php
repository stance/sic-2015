<?php
	require_once("../handler/youtube_handler.php");
	require_once("../handler/database_handler.php");
	require_once("../handler/submission_handler.php");
	
	
	if(isset($_POST['video_url']) AND $_POST['video_url']){
		$url = htmlspecialchars(trim($_POST['video_url']));
		$handle = new youtube_handler;
		$handle->create_instance($url);
		
		if($handle->fetch_title() == "incorrect_title"){
			echo "Invalid video title. The title should be of the form YOUR_INNOVATION_NAME - Stance Innovation Conclave 2015 (SIC)";
		}else{
			$submitter = new submission_handler;
			$submitter->set_url($handle->fetch_url());
			$submitter->set_title($handle->fetch_title());
			$submitter->set_views($handle->fetch_views());
			$submitter->set_submitted_by(htmlspecialchars(trim($_POST['user_id'])));
			if($submitter->submit() == "submission_complete"){
				echo "Your submission was successful. All the best - STANCE.";
			}else{
				echo "An unknown error has occured. You may mail the administrator at support@stance.world for assistance with submissions.";
			}
		}
	
	}
	else{
		echo "Please enter a valid URL";
	}

?>
