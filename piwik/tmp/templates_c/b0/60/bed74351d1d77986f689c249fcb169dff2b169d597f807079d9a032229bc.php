<?php

/* @Live/ajaxTotalVisitors.twig */
class __TwigTemplate_b060bed74351d1d77986f689c249fcb169dff2b169d597f807079d9a032229bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("@Live/_totalVisitors.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "@Live/ajaxTotalVisitors.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
