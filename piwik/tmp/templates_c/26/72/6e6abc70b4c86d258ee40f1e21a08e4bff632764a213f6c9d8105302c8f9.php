<?php

/* @CoreHome/checkForUpdates.twig */
class __TwigTemplate_26726e6abc70b4c86d258ee40f1e21a08e4bff632764a213f6c9d8105302c8f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("@CoreHome/_headerMessage.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "@CoreHome/checkForUpdates.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
