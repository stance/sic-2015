<?php

	session_start();

?>
<!--
 .d8888b.  888                                                                    
d88P  Y88b 888                                                                    
Y88b.      888                                                                    
 "Y888b.   888888  8888b.  88888b.   .d8888b .d88b.                               
    "Y88b. 888        "88b 888 "88b d88P"   d8P  Y8b                              
      "888 888    .d888888 888  888 888     88888888                              
Y88b  d88P Y88b.  888  888 888  888 Y88b.   Y8b.                                  
 "Y8888P"   "Y888 "Y888888 888  888  "Y8888P "Y8888                               
                                                                                  
                                                                                  
                                                                                  
8888888                                              888    d8b                   
  888                                                888    Y8P                   
  888                                                888                          
  888   88888b.  88888b.   .d88b.  888  888  8888b.  888888 888  .d88b.  88888b.  
  888   888 "88b 888 "88b d88""88b 888  888     "88b 888    888 d88""88b 888 "88b 
  888   888  888 888  888 888  888 Y88  88P .d888888 888    888 888  888 888  888 
  888   888  888 888  888 Y88..88P  Y8bd8P  888  888 Y88b.  888 Y88..88P 888  888 
8888888 888  888 888  888  "Y88P"    Y88P   "Y888888  "Y888 888  "Y88P"  888  888 
                                                                                  
                                                                                  
                                                                                  
 .d8888b.                             888                                         
d88P  Y88b                            888                                         
888    888                            888                                         
888         .d88b.  88888b.   .d8888b 888  8888b.  888  888  .d88b.               
888        d88""88b 888 "88b d88P"    888     "88b 888  888 d8P  Y8b              
888    888 888  888 888  888 888      888 .d888888 Y88  88P 88888888              
Y88b  d88P Y88..88P 888  888 Y88b.    888 888  888  Y8bd8P  Y8b.                  
 "Y8888P"   "Y88P"  888  888  "Y8888P 888 "Y888888   Y88P    "Y8888               
                                                                                  
                                                                                  
                                                                                  
 .d8888b.   .d8888b.   d888  888888888                                            
d88P  Y88b d88P  Y88b d8888  888                                                  
       888 888    888   888  888                                                  
     .d88P 888    888   888  8888888b.                                            
 .od888P"  888    888   888       "Y88b                                           
d88P"      888    888   888         888                                           
888"       Y88b  d88P   888  Y88b  d88P                                           
888888888   "Y8888P"  8888888 "Y8888P"   
-->
<!DOCTYPE html>
<html>
<head>
<title>Stance Innovation Conclave 2015 || STANCE</title>
<link href="./css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="./js/jquery.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="./css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="STANCE Innovation Conclave, Innovation Challenge, STANCE, SIC" />
<script type="application/x-javascript">addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Happy+Monkey' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Electrolize:300' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>



<!-- Start of recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- End of recaptcha -->

<!--Start of Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2u9qSbqgRjMPv1KwrsH5ChGkQZ6xrp0x";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Live Chat Script-->



<!-- slide -->
</head>
<body>
<!--header-->

	<div class="alert alert-success alert-dismissible reg-form" role="alert">
		<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="color:white">&times;</span><span class="sr-only">Close</span></button>
		<span id="result"></span>
	</div>
	<div class="header hidden-xs hidden-sm">
		<div class="container">	
			<div class="logo">
				<a href="index.php"><img style="margin-top:10px;" src="images/log.png" alt=""></a>
			</div>
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt=""> </span>
					<ul class="nav" >
						<li class="active" ><a href="#sic" >SIC</a></li>
						<li><a href="#overview" >COMPETITION OVERVIEW</a></li>
						<li><a href="#participation" >PARTICIPATE</a></li>
						<li><a href="#sponsors" >SPONSOR US</a></li>
						<li><a href="#reach-us" >REACH US</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

		<div class="header-small hidden-md hidden-lg visible-sm-block visible-xs-block">
		<div class="container">	
			<div class="logo">
				<a href="index.php"><img src="images/log.png" style="margin-top:10px;" alt=""></a>
			</div>
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt=""> </span>
					<ul class="nav" >
						<li class="active" ><a href="#sic" >SIC</a></li>
						<li><a href="#overview" >COMPETITION OVERVIEW</a></li>
						<li><a href="#participation" >PARTICIPATE</a></li>
						<li><a href="#sponsors" >SPONSOR US</a></li>
						<li><a href="#reach-us" >REACH US</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

<!---->

<div class="banner" id="sic">
		 <div class="container">
			<div class="col-md-8 banner-matter"id="ban">
				<!-- requried-jsfiles-for owl -->
				
							<div class="item-bottom">
					            <div class="item-right">
									<h1 id='heading'>Stance Innovation Conclave 2015</h1>
									<p class="by" id="para"><b>An initiative by - Society for Technical Advancements<br /> and Nationwide Computer Education.</b></p>
									<h2 style="padding-left:12em; margin-top:-30px;"><b>at</b></h2>
									<h2 style="padding-left:7em;"><b>G.B.P.U.A.&T., Pantnagar</b></h2>
									<br />
									<!-- Modal for full information -->
									<button  style="margin-left:20%;font-size:1.2em;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
									  How to participate?
									</button>
									
									<a href="invitation.php">
									<button type="button" style="margin-left:2%; font-size:1.2em;"  class="btn btn-success  ">
										Request Invite
									</button>
									</a>


									<!-- Modal -->
									<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									  <div class="modal-dialog">
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									        <h4 class="modal-title" id="myModalLabel"><b style="class:green">Stance Innovation Conclave 2015</b></h4>
									      </div>
									      <div class="modal-body">
									        <b class="text-success">It isn't all over; Everything has not been invented; The human adventure is just the beginning.</b><br><br>Come and feel the tranquility and show-off
									        your dexterity in <b class="text-success">STANCE Innovation Conclave 2015</b>. Enchant each and all with your genius while presenting your ideas on innovation and technology
									        <br><br>An entire new world is waiting for you and we provide you the way to highlight and feature yourself among the most elite group of individuals.
									        <br><br><b class="text-success">So why wait?</b> 
									        <br>	
									        <span class="text-success"><b>Click on the link below and be a part of it!</b></span>
									      </div>
									      <div class="modal-footer">
									        <a href="details.php">
												<button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#main-event">
												  Rules and Event Details
												</button>
											</a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									      </div>
									    </div>
									  </div>
									</div>	

									<!-- modal finishes -->
								</div>
							</div>								
						</div>
						<div class="col-md-4 hidden-xs hidden-sm" id="poster"style="">
							<img src='images/unnamed.jpg'>
						</div>
				</div>
				
				<div class="clearfix"> </div>
			</div>	
	</div>
	<!---->

	<!-- Competition overview -->


	<div class="content" id="overview">
		<div class="container">
			<div class="content-top">
				<div class="row">
					<div class="col-md-12">
						<h1 class="overview-title">Competition Overview</h1><br><br>
					</div>
					<div class="col-md-12">
						<p class="text-center overview-quote">"You have competition every day because you set such high standards for yourself that you
							have to go out every day and live up to that." - Michael Jordan</p>
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="col-md-12 grid-1">
						<h3>Introducing SIC</h3><br>
						<p class="overview-text">SIC is a national level innovation challenge  being held at <b>College of Technology, GBPUA.&T. Pantnagar</b> that invites students, developers, designers, marketers, product managers and startup enthusiasts together to
						share ideas, form teams, build products and geta chance to delve into the ocean of innovation.</p>
						<p class="overview-text">The motto of SIC is to discover the best ideas from across the nation that would drive the transformation
						and empowerment of society.  </p>
						<p class="overview-text">A key problem today is that the society at large still operates within outdated simile of the mind itself.All that is 
						needed is a newly minted idea, a belief and a vision to spark a revolution.</p>
						<br>
						<h3>Why SIC?</h3><br>
						<p class="overview-text">The real value of SIC is about learning through the act of creation, developing deeper relationships
						than a typical networking event, matching potential co-founders, validating ideas and experimenting in a risk-free environment, and for many, it's about 
						taking the first step into entrepreneurship.</p>
						<p class="overview-text">Have you always been inquisitive of the cutting edge technology and want an enriching experience while learning them?
						Or do you want to interact with students sharing common interest and dive into the subject. </p>
						<p class="overview-text">If YES, then Stance Innovation Conclave is for you.</p>
					</div>
					
				</div>
				<br><br>
				
			</div>
		</div>
	</div>
	<div class="separator"></div>
	<?php
	/*
	if(!isset($_SESSION['authenticated']) ){
	?>
	
	<div class="content-grid" id="participation">
		<h1 class="overview-title">Participate</h1>
		<p>Please fill out this form to confirm your participation in <b>SIC 2015</b></p>
		<br><br>
		<div class="row">
			<div class="col-md-4 col-md-offset-2" style="border-right:1px solid RGB(200,200,200);">
				<form action="register.php" method="POST" id="registerForm">
					<input type="text" name="name" class="form-control input-lg" placeholder="Name" required><br>
					<input type="email" name="email" class="form-control input-lg" placeholder="E-mail" required><br>
					<input type="password" name="password" class="form-control input-lg" placeholder="Password" required><br>
					<input type="password" name="confirm" class="form-control input-lg" placeholder="Confirm" required><br>
					<input type="submit" id="registerBtn" class="btn btn-primary btn-block btn-lg" value="Register">
				</form>
				
				

			</div>
			<div class="col-md-4">
				<span class="visible-xs-block visible-sm-block hidden-lg hidden-md"><br></span>

				<form action="login.php" method="POST" id="loginForm">
					<input type="email" name="email" class="form-control input-lg" placeholder="E-mail" required><br>
					<input type="password" name="password" class="form-control input-lg" placeholder="Password" required><br>
					<input type="submit" id="loginBtn" class="btn btn-primary btn-block btn-lg" value="login">

				</form>
			</div>
		</div>
		
	</div>
	
	<?php
	}
	else{
	?>
	<div class="content-grid" id="participation">
		<h1 class="overview-title">Participate</h1>
		<br><br><br><br><br><br><br><br><br><br>
		<div class="row">
			<a href="dashboard.php"><div class="btn btn-primary btn-block btn-lg">Return to dashboard</div></a>
		</div>
		<br><br>
	</div>
	
	<?php
	}*/
	?>
	
	<div class="content-grid" id="participation">
		<h1 class="overview-title">Participate</h1>
		<br><br><br><br><br><br><br><br><br><br>
		<div class="row">
			<a href="login.php"><div class="btn btn-success btn-block btn-lg">Register/Login</div></a>
		</div>
		<br><br>
	</div>
	
	
	<div class="content-grid" name="sponsors" id="sponsors">
		<h1 class="overview-title">Sponsor Us</h1><br />
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<a href="./brochure" target="_blank"><div class="btn btn-danger btn-block btn-lg"><b>Click here to know more about SIC - 2015</b></div></a>
			</div>
		</div>
		
		<br>
		<div class="row">
			<div class="col-md-4 col-md-offset-2" style="border-right:1px solid RGB(200,200,200)">
				<form action="sponsors.php" method="POST" id="sponsorsForm">
					<input type="text" name="name" class="form-control input-lg" placeholder="Name" required><br>
					<input type="text" name="org" class="form-control input-lg" placeholder="Organization" required><br>
					<input type="email" name="email" class="form-control input-lg" placeholder="E-mail" required><br>
					<input type="number" name="contact" class="form-control input-lg" placeholder="Contact number" required><br>
					<div class="g-recaptcha" data-sitekey="6LdtowQTAAAAAH-SwU_gKE9jsQkprs8WFucCFmma"></div>
					<input type="submit" id="sponsorsBtn"class="btn btn-primary btn-block btn-lg" value="Submit">
				</form>
			</div>
			<span class="visible-xs-block visible-sm-block hidden-lg hidden-md"><br></span>
			<div class="col-md-6 our-contact">
				<h2>For More Details Contact us:</h2><br>
				<h3>1) Shashank Dobhal</h3><br>
					<h4>+91 75 79 070424</h4><br>
				<h3>2) Shivam Verma</h3><br>
					<h4>+91 84 75 055645</h4>
				<br><h4>or shoot us an E-mail at<br> <b>admin@stance.world</b></h4>
			</div>
		</div><br />
		
		
	</div>
		


		<div class="content-bottom" id="reach-us">
			<div class="container">
				<h1 class="overview-title">Reach us</h1><br><br>
					<div class="row">
						<div class="col-md-6" id="googleMap" style="height:400px;">

						</div>
						<span class="visible-xs-block visible-sm-block hidden-lg hidden-md"><br></span>

						<div class="col-md-6">
							<!--
							<form action="query.php" method="POST" id="queryForm">
								<input type="text" name="name" class="form-control input-lg" placeholder="Name" required><br>
								<input type="email" name="email" class="form-control input-lg" placeholder="E-mail" required><br>
								<textarea name="query" class="form-control" placeholder="Enter your message here" style="height:188px;" required></textarea><br><br>
								<input type="submit" id="queryBtn" class="btn btn-primary btn-block btn-lg" value="Contact Now">
							</form>
							-->
							<br /><br /><br /><br /><br />
							<div class="panel panel-default">
							  <div class="panel-heading">
								<h3 class="panel-title"><h2>Contact us</h2></h3>
							  </div>
							  <div class="panel-body">
								<span>Drop a mail at:</span>
								<b>admin@stance.world</b>
								<br>
								OR<br />
								Call us: <b>Shashank Dobhal</b><br />
								<span><b>+91 75 79 070424</b></span>
							  </div>
							</div>

						</div>

					</div>

			</div>
		
		</div>

	</div>
	<!---->
	

	<div class="footer">
		<div class="container">
			
			<p class="footer-class">Copyright © 2015 <a href="http://www.our-stance.org/" target="_blank">STANCE</a> </p>
		</div>
		<script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});

						$(function() {
							  $('a[href*=#]:not([href=#])').click(function() {
							    
							    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
							      var target = $(this.hash);
							      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
							      if (target.length) {
							        $('html,body').animate({
							          scrollTop: target.offset().top
							        }, 1300);
							        return false;
							      }
							    }

							    
							  });
							});
					</script>
					<script src="http://maps.googleapis.com/maps/api/js"></script>
					<script>
					function initialize() {
					  var mapProp = {
					    center:new google.maps.LatLng(29.024276, 79.492929),
					    zoom:15,
					    mapTypeId:google.maps.MapTypeId.ROADMAP
					  };
					  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
					}
					google.maps.event.addDomListener(window, 'load', initialize);
					</script>

					<!-- Regarding Ajax form submission 
					<script src="js/script.js"></script>
						 -->
			
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	</div>
	<!-- Piwik -->
	<script type="text/javascript">
	  var _paq = _paq || [];
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
		var u="//stanceinnovationconclave.com/piwik/";
		_paq.push(['setTrackerUrl', u+'piwik.php']);
		_paq.push(['setSiteId', 1]);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<noscript><p><img src="//stanceinnovationconclave.com/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
	<!-- End Piwik Code -->



</body>
</html>
