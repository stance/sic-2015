$(function(){

	$("#registerForm").submit(function() {

	    var url = "register.php"; // the script where you handle the form input.

	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#registerForm").serialize(), // serializes the form's elements.
	           cache: false,
	           dataType:'text',
	           async:false,
			   beforeSend: function(){ $("#registerBtn").val('Registering...');},
	           success: function(data)
	           {

			   		  	
	            	var result = $.trim(data.toString());

	            	if( result==="done"){

	            		$("#registerBtn").val('Registered').attr('disabled','true');

	            		$('.reg-form').html('<strong>Congrats,</strong> You are now a registered member. Login to open your personal Dashboard').show();
	            	}    
	            	if( result==="duplicateEmail"){

	            		$("#registerBtn").val('Register');
	            		$('.reg-form').html('<strong>Sorry,</strong> E-mail already registered with us').show();
	            		
	            	}
	            	if( result==="diffpass"){

	            		$("#registerBtn").val('Register');
	            		$('.reg-form').html('<strong>Error:</strong> Passwords do not match').show();

	            	}
	            	
	              
	           }
	         });

	    return false; // avoid to execute the actual submit of the form.
	});


	$("#loginForm").submit(function() {

	    var url = "login.php"; // the script where you handle the form input.

	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#loginForm").serialize(), // serializes the form's elements.
	           cache: false,
	           dataType:'text',
	           async:false,
			   beforeSend: function(){ $("#loginBtn").val('Signing in...');},
	           success: function(data)
	           {
	            	
	            	var result = $.trim(data.toString());

	            	if( result==='success'){

			   			$("#loginBtn").attr('disabled','true');
			   			$("#loginBtn").val('logged in');
	            		
	            		$(location).attr('href','dashboard.php');
	            	}    
	            	else{

			   			$("#loginBtn").val('Try again');
	            		$('.reg-form').html('<strong>Error:</strong> Invalid username or password').show();
	            		
	            	}    
	              
	           }
	         });

	    return false; // avoid to execute the actual submit of the form.
	});



	$("#sponsorsForm").submit(function() {

	    var url = "sponsors.php"; // the script where you handle the form input.

	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#sponsorsForm").serialize(), // serializes the form's elements.
	           cache: false,
	           dataType:'text',
	           async:false,
			   beforeSend: function(){ $("#sponsorsBtn").val('Submitting Details...');},
	           success: function(data)
	           {
			   		var result = $.trim(data.toString());

			   		if( result==='thanks'){

			   			$("#sponsorsBtn").val('Done...');
			   			$("#sponsorsBtn").attr('disabled','true');

	            		$('.reg-form').html('<strong>Thank you,</strong> We will contact you soon.').show();


			   		}
			   		if( result==='failed'){

			   			$("#sponsorsBtn").val('Try again...');
	            		$('.reg-form').html('<strong>Sorry,</strong> Something went wrong.').show();
			   		}    
	              
	           }
	         });

	    return false; // avoid to execute the actual submit of the form.
	});

	$("#queryForm").submit(function() {

	    var url = "query.php"; // the script where you handle the form input.

	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#queryForm").serialize(), // serializes the form's elements.
	           cache: false,
	           dataType:'text',
	           async:false,
			   beforeSend: function(){ $("#queryBtn").val('Submitting Query...');},
	           success: function(data)
	           {
			   		var result = $.trim(data.toString());
			   		if( result==='thanks'){

			   			$("#queryBtn").val('Done...');
			   			$("#queryBtn").attr('disabled','true');

	            		$('.reg-form').html('<strong>Thank you,</strong> We will contact you soon.').show();


			   		}
			   		if( result==='failed'){

			   			$("#queryBtn").val('Try again...');
	            		$('.reg-form').html('<strong>Sorry,</strong> Something went wrong.').show();
			   		}  
			   		
	           }

	         });

	    return false; // avoid to execute the actual submit of the form.
	});

	$(function(){

		$(document).click(function(){

			$('.reg-form').fadeOut('1500');
		})
	});

});

		