<?php
	//This is the video handler for our database
	
	class db_video_handler extends database_handler{
		private $con;
		private $valid_id;
		private $video_id;
		private $video_title;
		private $video_url;
		private $video_views;
		private $video_submitted_by_id;
		private $video_submitted_by_name;
		private $video_daily_ranking;
		private $video_all_time_ranking;
		
		function __construct(){
			//$this->valid_id = 0;
			$this->con = parent::connect_to_db();
			$this->video_all_time_ranking 	= 999;
			$this->video_daily_ranking		= 999;
		}
		
		public function fetch_details_from_id($vid){
			$this->video_id = $vid;
			$sql = "SELECT submissions.video_id, submissions.url, submissions.title, submissions.views, participants.id, participants.name FROM participants INNER JOIN submissions ON participants.email=submissions.submitted_by WHERE submissions.video_id=".$this->video_id."";
			$result = mysqli_query($this->con, $sql);
			if($result->num_rows){
				$this->valid_id = 1;
				while($row = $result->fetch_assoc()) {        
					$this->video_id					= $row['video_id'];
					$this->video_submitted_by_id 	= $row['id'];
					$this->video_submitted_by_name 	= $row['name'];
					$this->video_title				= $row['title'];
					$this->video_url				= $row['url'];
					$this->video_views				= $row['views'];
				}
			}
			else{
				$this->valid_id = 0;
			}
			
		}		
		
		public function fetch_title(){
			return $this->video_title;
		}
		
		public function fetch_id(){
			return $this->video_id;
		}
		
		public function fetch_url(){
			return $this->video_url;
		}
		
		public function fetch_views(){
			return $this->video_views;
		}
		
		public function fetch_user_id(){
			return $this->video_submitted_by_id;
		}
		
		public function fetch_user_name(){
			return $this->video_submitted_by_name;
		}
		
		public function fetch_daily_ranking(){
			return $this->video_daily_ranking;
		}
		
		public function fetch_all_time_ranking(){
			return $this->video_all_time_ranking;
		}
		
		public function fetch_embed_url(){
			$this->video_url = htmlspecialchars(trim($this->video_url));
			parse_str( parse_url( $this->video_url, PHP_URL_QUERY ), $segments );
			$id = $segments['v'];
			return "http://www.youtube.com/v/".$id;
		}
		
		public function is_valid(){
			return $this->valid_id;
		}
	}
?>
