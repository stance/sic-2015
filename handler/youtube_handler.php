<?php
	class youtube_handler{
			private $valid;
			private $url;
			private $id;
			private $title;
			private $views;
			
			public function create_instance($url){
					$this->url = htmlspecialchars(trim($url));
					parse_str( parse_url( $this->url, PHP_URL_QUERY ), $segments );
					$this->id = $segments['v'];
					$this->views = $this->update_views($this->id);
					$temporary_title = $this->update_title($this->id);
					if($this->verify_title($temporary_title)){
						$this->title = $this->refine_title($temporary_title);
						$this->valid = 1;
					}else{
						$this->title = "incorrect_title";
						$this->valid = 0;
					}
			}
			
			//This function will verify that whether or not the video is eligible for participation in SIC 2015
			private function verify_title($temporary_title){
				$splitter = explode("- ", $temporary_title);
				if(array_pop($splitter) == "Stance Innovation Conclave 2015 (SIC)"){
					return 1;
				}
				else{
					return 0;
				}
			}
			
			//This function will prepare the title for storage
			private function refine_title($temporary_title){
				$splitter = explode("- ", $temporary_title);
				array_pop($splitter);
				$rejoined = implode("- ", $splitter);
				return $rejoined;
			}
			
			//Fetch views of the video
			private function update_views($video_ID){
				 //= 'FoiHX9azZeQ';
				$JSON = @file_get_contents("https://gdata.youtube.com/feeds/api/videos/{$video_ID}?v=2&alt=json");
				$JSON_Data = json_decode($JSON);
				$views = $JSON_Data->{'entry'}->{'yt$statistics'}->{'viewCount'};
				return $views;
			}
			
			//Fetch title of the video
			private function update_title($video_ID){
				$content = @file_get_contents("http://youtube.com/get_video_info?video_id=".$video_ID);
				parse_str($content, $ytarr);
				$title = $ytarr['title'];
				return $title;
			}
			
			//Tells the validity of Title
			public function is_valid(){
				return $this->valid;
			}
			
			public function fetch_views(){
				return $this->views;
			}
			
			public function fetch_title(){
				return $this->title;
			}
			
			public function fetch_url(){
				return $this->url;
			}
	}
	
?>
