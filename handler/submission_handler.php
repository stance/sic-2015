<?php
	class submission_handler extends database_handler{
		private $con;
		private $id;
		private $url;
		private $submitted_by;
		private $views;
		private $title;
		
		function __construct(){
			$this->con = parent::connect_to_db();
			$this->fetch_id_from_db();
		}
				
		private function fetch_id_from_db(){
			$sql = "SELECT counter_value FROM counter WHERE counter_name='submission_number'";
			$result = mysqli_query($this->con, $sql);
			while($row = $result->fetch_assoc()) {        
				$this->id = $row['counter_value'] + 1;
			}
			return $this->id;
		}
		
		private function update_counter(){
			$sql = "UPDATE counter SET counter_value=".$this->id." WHERE counter_name='submission_number'";
			$result = mysqli_query($this->con, $sql);
			if($result){
				return 1;
			}
			else{
				return 0;
			}
		}
		
		public function set_views($views){
			$this->views = htmlspecialchars(trim($views));
		}
		
		public function set_title($title){
			$this->title = htmlspecialchars(trim($title));
		}
		
		public function set_submitted_by($submitted_by){
			$this->submitted_by	= htmlspecialchars(trim($submitted_by));
		}
		
		public function set_url($url){
			$this->url = htmlspecialchars(trim($url));
		}
		
		public function submit(){
			$sql = "INSERT INTO submissions (video_id, url, submitted_by, views, title) VALUES (".$this->id.", '".$this->url."', '".$this->submitted_by."', ".$this->views.", '".$this->title."')";
			$result = mysqli_query($this->con, $sql);
			if($result){
				$sql2 = "INSERT INTO all_time_ranking (submission_id, views, rank) VALUES (".$this->id.", 999, 999)";
				$result2 = mysqli_query($this->con, $sql2);
				$this->update_counter();
				return "submission_complete";
			}
			else{
				return "error";
			}
		}
	}

?>
