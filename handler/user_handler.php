<?php
	//This class will handle all user interactions
	class user_handler extends database_handler{
		private $con;
		private $user_id;
		private $user_name;
		private $valid_id;
		private $user_email;
		
		function __construct(){
			$this->con = parent::connect_to_db();
		}
		
		public function retrieve_from_id($id){
			$this->user_id = $id;
			$sql = "SELECT * FROM participants WHERE id='".$this->user_id."'";
			$result = mysqli_query($this->con, $sql);
			if($result->num_rows){
				$this->valid_id = 1;
			}
			else{
				$this->valid_id = 0;
			}
			while($row = $result->fetch_assoc()) {        
				$this->user_name = $row['name'];
				$this->user_email = $row['email'];
			}
		}
		
		public function retrieve_from_email($email){
			$this->user_email = $email;
			$sql = "SELECT * FROM participants WHERE email='".$this->user_email."'";
			$result = mysqli_query($this->con, $sql);
			if($result->num_rows){
				$this->valid_id = 1;
			}
			else{
				$this->valid_id = 0;
			}
			while($row = $result->fetch_assoc()) {        
				$this->user_name = $row['name'];
				$this->user_id = $row['id'];
			}
		}
		
		public function is_valid(){
			return $this->valid_id;
		}
		
		//Function for retrieving all submissions of a particular user
		public function retrieve_submissions_from_email($email){
			$sql = "SELECT submissions.video_id, submissions.url, submissions.title, submissions.views, participants.id, participants.name, participants.email FROM participants INNER JOIN submissions ON participants.email=submissions.submitted_by WHERE submissions.submitted_by='".$email."';";
			$this->submissions = mysqli_query($this->con, $sql);
			return $this->submissions;
		}
		
		//Function for retrieving rankings of all videos
		public function retrieve_rankings(){
			$sql = "SELECT submissions.video_id, submissions.url, submissions.title, submissions.views, participants.id, participants.name, participants.email FROM participants INNER JOIN submissions ON participants.email=submissions.submitted_by ORDER BY submissions.views DESC";
			$this->submissions = mysqli_query($this->con, $sql);
			return $this->submissions;
		}

		
		public function fetch_name(){
			return $this->user_name;
		}
		
		public function fetch_id(){
			return $this->user_id;
		}
		
		public function fetch_email(){
			return $this->user_email;
		}
		
		//Just for testing conenctions
		public function dump_con(){
			var_dump(parent::connect_to_db());
		}
	}
?>
