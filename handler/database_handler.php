<?php
	//This class will handle most of the database connections and needs to be extended in order to be user
	//No direct usage or initialization is permitted
	
	abstract class database_handler{
		private function connect_to_server(){
			$server_name 	= "localhost";
			$user_name		= "keylopw6_shubham";
			$password		= "Aster#42073";
			$database_name	= "keylopw6_stance_pages";
			$connection_to_db = mysqli_connect($server_name, $user_name, $password, $database_name) or die("couldn't connect to DB");
			return $connection_to_db;
		}
		
		private function connect_to_local_machine(){
			$server_name 	= "localhost";
			$user_name		= "root";
			$password		= "9897132475";
			$database_name	= "keylopw6_stance_pages";
			$connection_to_db = mysqli_connect($server_name, $user_name, $password, $database_name) or die("couldn't connect to DB");
			return $connection_to_db;
		}
		
		function connect_to_db(){
			$environment = "local";
			if($environment == "local"){
				$connection = $this->connect_to_local_machine();
			}
			else{
				$connection = $this->connect_to_server();
			}
			return $connection;
		}
	}
?>
