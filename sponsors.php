<?php

	session_start();

?>
<!--
 .d8888b.  888                                                                    
d88P  Y88b 888                                                                    
Y88b.      888                                                                    
 "Y888b.   888888  8888b.  88888b.   .d8888b .d88b.                               
    "Y88b. 888        "88b 888 "88b d88P"   d8P  Y8b                              
      "888 888    .d888888 888  888 888     88888888                              
Y88b  d88P Y88b.  888  888 888  888 Y88b.   Y8b.                                  
 "Y8888P"   "Y888 "Y888888 888  888  "Y8888P "Y8888                               
                                                                                  
                                                                                  
                                                                                  
8888888                                              888    d8b                   
  888                                                888    Y8P                   
  888                                                888                          
  888   88888b.  88888b.   .d88b.  888  888  8888b.  888888 888  .d88b.  88888b.  
  888   888 "88b 888 "88b d88""88b 888  888     "88b 888    888 d88""88b 888 "88b 
  888   888  888 888  888 888  888 Y88  88P .d888888 888    888 888  888 888  888 
  888   888  888 888  888 Y88..88P  Y8bd8P  888  888 Y88b.  888 Y88..88P 888  888 
8888888 888  888 888  888  "Y88P"    Y88P   "Y888888  "Y888 888  "Y88P"  888  888 
                                                                                  
                                                                                  
                                                                                  
 .d8888b.                             888                                         
d88P  Y88b                            888                                         
888    888                            888                                         
888         .d88b.  88888b.   .d8888b 888  8888b.  888  888  .d88b.               
888        d88""88b 888 "88b d88P"    888     "88b 888  888 d8P  Y8b              
888    888 888  888 888  888 888      888 .d888888 Y88  88P 88888888              
Y88b  d88P Y88..88P 888  888 Y88b.    888 888  888  Y8bd8P  Y8b.                  
 "Y8888P"   "Y88P"  888  888  "Y8888P 888 "Y888888   Y88P    "Y8888               
                                                                                  
                                                                                  
                                                                                  
 .d8888b.   .d8888b.   d888  888888888                                            
d88P  Y88b d88P  Y88b d8888  888                                                  
       888 888    888   888  888                                                  
     .d88P 888    888   888  8888888b.                                            
 .od888P"  888    888   888       "Y88b                                           
d88P"      888    888   888         888                                           
888"       Y88b  d88P   888  Y88b  d88P                                           
888888888   "Y8888P"  8888888 "Y8888P"   
-->
<!DOCTYPE html>
<html>
<head>
<title>Stance Innovation Conclave 2015 || STANCE</title>
<link href="./css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="./js/jquery.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="./css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="STANCE Innovation Conclave, Innovation Challenge, STANCE, SIC" />
<script type="application/x-javascript">addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Happy+Monkey' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Electrolize:300' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!-- slide -->

<!--Start of Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2u9qSbqgRjMPv1KwrsH5ChGkQZ6xrp0x";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Live Chat Script-->

<!-- Start of recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- End of recaptcha -->
</head>
<body >
<!--header-->

	<div class="alert alert-success alert-dismissible reg-form" role="alert">
		<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="color:white">&times;</span><span class="sr-only">Close</span></button>
		<span id="result"></span>
	</div>
	<div class="header hidden-xs hidden-sm">
		<div class="container">	
			<div class="logo">
				<a href="index.php"><img style="margin-top:10px;" src="images/log.png" alt=""></a>
			</div>
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt=""> </span>
					<ul class="nav" >
						<li class="active" ><a href="index.php" >SIC</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

		<div class="header-small hidden-md hidden-lg visible-sm-block visible-xs-block">
		<div class="container">	
			<div class="logo">
				<a href="index.php"><img src="images/log.png" style="margin-top:10px;" alt=""></a>
			</div>
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt=""> </span>
					<ul class="nav" >
						<li class="active" ><a href="index.php" >SIC</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

<!---->

<?php 

        if(isset($_POST['g-recaptcha-response'])){
          $captcha=$_POST['g-recaptcha-response'];
        }
        if(!$captcha){
		?>
			<div class="content-grid" name="sponsors" id="sponsors">
					<h3 class="overview-title">Missing captcha. Please fill the form again or contact us directly.</h3><br />
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<a href="./brochure" target="_blank"><div class="btn btn-danger btn-block btn-lg"><b>Click here to know more about SIC - 2015</b></div></a>
						</div>
					</div>
					
					<br>
					<div class="row">
						<div class="col-md-4 col-md-offset-2" style="border-right:1px solid RGB(200,200,200)">
							<form action="sponsors.php" method="POST" id="sponsorsForm">
								<input type="text" name="name" class="form-control input-lg" placeholder="Name" required><br>
								<input type="text" name="org" class="form-control input-lg" placeholder="Organization" required><br>
								<input type="email" name="email" class="form-control input-lg" placeholder="E-mail" required><br>
								<input type="number" name="contact" class="form-control input-lg" placeholder="Contact number" required><br>
								<div class="g-recaptcha" data-sitekey="6LdtowQTAAAAAH-SwU_gKE9jsQkprs8WFucCFmma"></div>
								<input type="submit" id="sponsorsBtn"class="btn btn-primary btn-block btn-lg" value="Submit">
							</form>
						</div>
						<span class="visible-xs-block visible-sm-block hidden-lg hidden-md"><br></span>
						<div class="col-md-6 our-contact">
							<h2>For More Details Contact us:</h2><br>
							<h3>1) Shashank Dobhal</h3><br>
								<h4>+91 75 79 070424</h4><br>
							<h3>2) Shivam Verma</h3><br>
								<h4>+91 84 75 055645</h4>
							<br><h4>or shoot us an E-mail at<br> <b>admin@stance.world</b></h4>
						</div>
					</div><br />
					
					
				</div>
		<?php
        }
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LdtowQTAAAAAFrzHNgE5GGxmpaB-YRjC4B-dsRb&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
        if($response.success==false)
        {
		?>
			<div class="content-grid" name="sponsors" id="sponsors">
					<h1 class="overview-title">Incorrect captcha. Please fill the form again or contact us directly.</h1><br />
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<a href="./brochure" target="_blank"><div class="btn btn-danger btn-block btn-lg"><b>Click here to know more about SIC - 2015</b></div></a>
						</div>
					</div>
					
					<br>
					<div class="row">
						<div class="col-md-4 col-md-offset-2" style="border-right:1px solid RGB(200,200,200)">
							<form action="sponsors.php" method="POST" id="sponsorsForm">
								<input type="text" name="name" class="form-control input-lg" placeholder="Name" required><br>
								<input type="text" name="org" class="form-control input-lg" placeholder="Organization" required><br>
								<input type="email" name="email" class="form-control input-lg" placeholder="E-mail" required><br>
								<input type="number" name="contact" class="form-control input-lg" placeholder="Contact number" required><br>
								<div class="g-recaptcha" data-sitekey="6LdtowQTAAAAAH-SwU_gKE9jsQkprs8WFucCFmma"></div>
								<input type="submit" id="sponsorsBtn"class="btn btn-primary btn-block btn-lg" value="Submit">
							</form>
						</div>
						<span class="visible-xs-block visible-sm-block hidden-lg hidden-md"><br></span>
						<div class="col-md-6 our-contact">
							<h2>For More Details Contact us:</h2><br>
							<h3>1) Shashank Dobhal</h3><br>
								<h4>+91 75 79 070424</h4><br>
							<h3>2) Shivam Verma</h3><br>
								<h4>+91 84 75 055645</h4>
							<br><h4>or shoot us an E-mail at<br> <b>admin@stance.world</b></h4>
						</div>
					</div><br />
					
					
				</div>
		<?php
        }else
        {
        
				if( isset($_POST['name']) ){

					$con = mysqli_connect("localhost","keylopw6_shubham", "Aster#42073", "keylopw6_stance_pages") or die("couldn't connect to DB");
					
					$sql = "CREATE TABLE IF NOT EXISTS sponsors(
							id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
							name VARCHAR(50) NOT NULL,
							email VARCHAR(50),
							org VARCHAR(50),
							contact VARCHAR(13)
							)";

					mysqli_query($con, $sql) or die(mysqli_error($con));

					$name = htmlspecialchars(trim( $_POST['name']) );
					$org = htmlspecialchars(trim( $_POST['org']) );
					$email = htmlspecialchars(trim( $_POST['email']) );
					$contact = htmlspecialchars(trim($_POST['contact']) );

					$sql = "INSERT INTO sponsors (name,org,email,contact) VALUES ('$name','$org','$email','$contact')";

					if( mysqli_query($con, $sql) ){

			?>
						<div class="content-grid" name="sponsors" id="sponsors">
								<h1 class="overview-title">Thank you</h1><br />
								<div class="row">
									<div class="col-md-8 col-md-offset-2">
										<a href="./brochure" target="_blank"><div class="btn btn-danger btn-block btn-lg"><b>Click here to know more about SIC - 2015</b></div></a>
									</div>
								</div>
								
								<br>
								<div class="row">
									<div class="col-md-4 col-md-offset-2" style="border-right:1px solid RGB(200,200,200)">
										<h3 class="overview-title">We will get in touch with you within 24 hours.</h3>
									</div>
									<span class="visible-xs-block visible-sm-block hidden-lg hidden-md"><br></span>
									<div class="col-md-6 our-contact">
										<h2>For More Details Contact us:</h2><br>
										<h3>1) Shashank Dobhal</h3><br>
											<h4>+91 75 79 070424</h4><br>
										<h3>2) Shivam Verma</h3><br>
											<h4>+91 84 75 055645</h4>
										<br><h4>or shoot us an E-mail at<br> <b>admin@stance.world</b></h4>
									</div>
								</div><br />
								
								
							</div>
					<?php
        
					}
					else{

						echo "failed";
					}
					mysqli_close($con);

				}
				else{

					header("location:index.php");
				}
				
			}
?>

<?php
?>	

<!---->
	

	<div class="footer">
		<div class="container">
			
			<p class="footer-class">Copyright © 2015 <a href="http://www.our-stance.org/" target="_blank">STANCE</a> </p>
		</div>
								
				
	</div>
	<script src="./js/user2.js"></script>
</body>
</html>


	
