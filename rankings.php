<?php
	require_once("handler/database_handler.php");
	require_once("handler/user_handler.php");
	session_start();
	//$_SESSION['authenticated'] = 1;
	//$_SESSION['email'] = "reader@asds";
	if( isset($_SESSION['authenticated']) ){
		$current_user = new user_handler;
		$current_user->retrieve_from_email($_SESSION['email']);
?>
<!--
 .d8888b.  888                                                                    
d88P  Y88b 888                                                                    
Y88b.      888                                                                    
 "Y888b.   888888  8888b.  88888b.   .d8888b .d88b.                               
    "Y88b. 888        "88b 888 "88b d88P"   d8P  Y8b                              
      "888 888    .d888888 888  888 888     88888888                              
Y88b  d88P Y88b.  888  888 888  888 Y88b.   Y8b.                                  
 "Y8888P"   "Y888 "Y888888 888  888  "Y8888P "Y8888                               
                                                                                  
                                                                                  
                                                                                  
8888888                                              888    d8b                   
  888                                                888    Y8P                   
  888                                                888                          
  888   88888b.  88888b.   .d88b.  888  888  8888b.  888888 888  .d88b.  88888b.  
  888   888 "88b 888 "88b d88""88b 888  888     "88b 888    888 d88""88b 888 "88b 
  888   888  888 888  888 888  888 Y88  88P .d888888 888    888 888  888 888  888 
  888   888  888 888  888 Y88..88P  Y8bd8P  888  888 Y88b.  888 Y88..88P 888  888 
8888888 888  888 888  888  "Y88P"    Y88P   "Y888888  "Y888 888  "Y88P"  888  888 
                                                                                  
                                                                                  
                                                                                  
 .d8888b.                             888                                         
d88P  Y88b                            888                                         
888    888                            888                                         
888         .d88b.  88888b.   .d8888b 888  8888b.  888  888  .d88b.               
888        d88""88b 888 "88b d88P"    888     "88b 888  888 d8P  Y8b              
888    888 888  888 888  888 888      888 .d888888 Y88  88P 88888888              
Y88b  d88P Y88..88P 888  888 Y88b.    888 888  888  Y8bd8P  Y8b.                  
 "Y8888P"   "Y88P"  888  888  "Y8888P 888 "Y888888   Y88P    "Y8888               
                                                                                  
                                                                                  
                                                                                  
 .d8888b.   .d8888b.   d888  888888888                                            
d88P  Y88b d88P  Y88b d8888  888                                                  
       888 888    888   888  888                                                  
     .d88P 888    888   888  8888888b.                                            
 .od888P"  888    888   888       "Y88b                                           
d88P"      888    888   888         888                                           
888"       Y88b  d88P   888  Y88b  d88P                                           
888888888   "Y8888P"  8888888 "Y8888P"   
-->

		<!DOCTYPE HTML>

		<html>
			<head>
				<meta charset="UTF-8">
				<title>Rankings: Most Popular Video || SIC - 2015</title>

				<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
				<script src="./js/jquery.min.js"></script>
				<script src="./js/bootstrap.min.js"></script>
				<!-- Custom Theme files -->
				<!--theme-style-->
				<link href="./css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />	

				<link href="./css/style.css" rel="stylesheet" type="text/css" media="all" />	
				<!--//theme-style-->
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<meta name="keywords" content="STANCE Innovation Conclave, Innovation Challenge, STANCE, SIC" />
				<script type="application/x-javascript">addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
			
							
				<!--Start of Live Chat Script-->
				<script type="text/javascript">
				window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
				d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
				_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
				$.src="//v2.zopim.com/?2u9qSbqgRjMPv1KwrsH5ChGkQZ6xrp0x";z.t=+new Date;$.
				type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
				</script>
				<!--End of Live Chat Script-->

				
			
			</head>

			<body>
				<header class="container-fluid">
					<div class="row">
						<div class="col-md-12 status-bar" style="height:70px;">
							<div class="col-md-2 col-md-offset-1 logo">
								<img src="images/log.png">
							</div>

							<div class="col-md-9">
								
								<div class="row">
									<div class="pull-right col-md-2 account-link">
										<div class="dropdown">
										  <button id="dLabel" type="button" class="account-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										    <?php echo $current_user->fetch_name();?>
										    <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel" >
											<a href="dashboard.php"><li><span style="padding-left:10px">Dashboard</span></li></a>
										    <li class="divider"></li>
										    <a href="mailto:admin@stance.world"><li><span style="padding-left:10px">Contact Us</span></li></a>
										    <li class="divider"></li>
										    <a href="details.php"><li><span style="padding-left:10px">FAQs</span></li></a>
										    <li class="divider"></li>
										    <a href="logout.php"><li><span style="padding-left:10px">Sign Out</span></li></a>
										  </ul>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="row" style="padding-top:20px;">
						<div class="col-md-4 col-md-offset-4">
							 <div class="btn-group " >
							  <a href="dashboard.php"><button type="button" class="col-md-4 btn btn-primary">Dashboard</button></a>
							  <a href="rankings.php"><button type="button" class="col-md-4 btn btn-primary">Rankings</button></a>
							  <a href="invitation.php"><button type="button" class="col-md-4 btn btn-success  ">Request Invite</button></a>
							</div>
						</div>
						<br /><br />
					</div>
				</header>
				<br>
				<section class='col-md-offset-1'>
					<div class="page-header">
						<h2>Rankings <small>Most Popular Video</small></h2>
					</div>
						<div class="row" style="padding:3px 5px;">
							<div class="col-md-1"><b>Rank</b></div>
							<div class="col-md-5"><b>Title</b></div>
							<div class="col-md-3"><b>Submitted By</b></div></a>
							<div class="col-md-3"><b>Views</b></div>
						</div>

					<?php
					//This is the code for retrieval and display of rankings
						$result = $current_user->retrieve_rankings();
						$i = 1;
						while($row = $result->fetch_assoc()) {        
					?>
						<div class="row" style="padding:3px 5px;">
							<a href="profile.php?pid=<?php echo $row['id'];?>&vid=<?php echo $row['video_id'];?>">
								<div class="col-md-1">#<?php echo $i;?></div>
								<div class="col-md-5"><?php echo $row['title'];?></div>
							</a>
							<a href="profile.php?pid=<?php echo $row['id'];?>"><div class="col-md-3"><?php echo $row['name'];?></div></a>
							<div class="col-md-3"><?php echo $row['views'];?></div>
						</div>
					<?php
						$i = $i + 1;
						}
			
					?>
					
					

				</section>
				
				<br>
				<div class="footer">
					<div class="container">
						
						<p class="footer-class">Copyright © 2015 <a href="http://www.stance.world/" target="_blank">STANCE</a> </p>
					</div>
				</div>
				
				<script src="./js/user.js"></script>
				<!-- Piwik -->
				<script type="text/javascript">
				  var _paq = _paq || [];
				  _paq.push(['trackPageView']);
				  _paq.push(['enableLinkTracking']);
				  (function() {
					var u="//stanceinnovationconclave.com/piwik/";
					_paq.push(['setTrackerUrl', u+'piwik.php']);
					_paq.push(['setSiteId', 1]);
					var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
					g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
				  })();
				</script>
				<noscript><p><img src="//stanceinnovationconclave.com/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
				<!-- End Piwik Code -->

			
			
			</body>

		</html>

		

<?php
	}
	else{
		header("location:index.php");
	}

?>
