<?php

	session_start();

?>
<!--
 .d8888b.  888                                                                    
d88P  Y88b 888                                                                    
Y88b.      888                                                                    
 "Y888b.   888888  8888b.  88888b.   .d8888b .d88b.                               
    "Y88b. 888        "88b 888 "88b d88P"   d8P  Y8b                              
      "888 888    .d888888 888  888 888     88888888                              
Y88b  d88P Y88b.  888  888 888  888 Y88b.   Y8b.                                  
 "Y8888P"   "Y888 "Y888888 888  888  "Y8888P "Y8888                               
                                                                                  
                                                                                  
                                                                                  
8888888                                              888    d8b                   
  888                                                888    Y8P                   
  888                                                888                          
  888   88888b.  88888b.   .d88b.  888  888  8888b.  888888 888  .d88b.  88888b.  
  888   888 "88b 888 "88b d88""88b 888  888     "88b 888    888 d88""88b 888 "88b 
  888   888  888 888  888 888  888 Y88  88P .d888888 888    888 888  888 888  888 
  888   888  888 888  888 Y88..88P  Y8bd8P  888  888 Y88b.  888 Y88..88P 888  888 
8888888 888  888 888  888  "Y88P"    Y88P   "Y888888  "Y888 888  "Y88P"  888  888 
                                                                                  
                                                                                  
                                                                                  
 .d8888b.                             888                                         
d88P  Y88b                            888                                         
888    888                            888                                         
888         .d88b.  88888b.   .d8888b 888  8888b.  888  888  .d88b.               
888        d88""88b 888 "88b d88P"    888     "88b 888  888 d8P  Y8b              
888    888 888  888 888  888 888      888 .d888888 Y88  88P 88888888              
Y88b  d88P Y88..88P 888  888 Y88b.    888 888  888  Y8bd8P  Y8b.                  
 "Y8888P"   "Y88P"  888  888  "Y8888P 888 "Y888888   Y88P    "Y8888               
                                                                                  
                                                                                  
                                                                                  
 .d8888b.   .d8888b.   d888  888888888                                            
d88P  Y88b d88P  Y88b d8888  888                                                  
       888 888    888   888  888                                                  
     .d88P 888    888   888  8888888b.                                            
 .od888P"  888    888   888       "Y88b                                           
d88P"      888    888   888         888                                           
888"       Y88b  d88P   888  Y88b  d88P                                           
888888888   "Y8888P"  8888888 "Y8888P"   
-->
<!DOCTYPE html>
<html>
<head>
<title>Stance Innovation Conclave 2015 || STANCE</title>
<link href="./css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="./js/jquery.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="./css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="STANCE Innovation Conclave, Innovation Challenge, STANCE, SIC" />
<script type="application/x-javascript">addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Happy+Monkey' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Electrolize:300' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!-- slide -->

<!--Start of Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2u9qSbqgRjMPv1KwrsH5ChGkQZ6xrp0x";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Live Chat Script-->

<!-- Start of recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- End of recaptcha -->
</head>
<body >
<!--header-->

	<div class="alert alert-success alert-dismissible reg-form" role="alert">
		<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="color:white">&times;</span><span class="sr-only">Close</span></button>
		<span id="result"></span>
	</div>
	<div class="header hidden-xs hidden-sm">
		<div class="container">	
			<div class="logo">
				<a href="index.php"><img style="margin-top:10px;" src="images/log.png" alt=""></a>
			</div>
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt=""> </span>
					<ul class="nav" >
						<li class="active" ><a href="index.php" >SIC</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

		<div class="header-small hidden-md hidden-lg visible-sm-block visible-xs-block">
		<div class="container">	
			<div class="logo">
				<a href="index.php"><img src="images/log.png" style="margin-top:10px;" alt=""></a>
			</div>
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt=""> </span>
					<ul class="nav" >
						<li class="active" ><a href="index.php" >SIC</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

<!---->


	<?php
	
	if(!isset($_SESSION['authenticated']) ){
	?>
	
	<div class="content-grid" id="participation" style='margin-bottom:-30px;margin-top:-10px;'>
		<h1 class="overview-title">Participate</h1>
		<p>Please fill out this form to confirm your participation in <b>SIC 2015</b></p>
		<br><br>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<form action="register.php" method="POST" id="registerForm">
					<input type="text" name="name" class="form-control input-lg" placeholder="Name" required><br>
					<input type="email" name="email" class="form-control input-lg" placeholder="E-mail" required><br>
					<input type="password" name="password" class="form-control input-lg" placeholder="Password" required><br>
					<input type="password" name="confirm" class="form-control input-lg" placeholder="Confirm" required><br>
					<div class="g-recaptcha" data-theme="dark" data-sitekey="6LdtowQTAAAAAH-SwU_gKE9jsQkprs8WFucCFmma"></div>
					<input type="submit" id="registerBtn" class="btn btn-primary btn-block btn-lg" value="Register">
				</form>
				
								
				<br>
				<a href="login.php">
					<div class="btn btn-warning btn-lg col-md-12">Registered User? Click here to login</div>
				</a>
			</div>
		</div>
		
	</div>
	
	<?php
	}
	else{
	?>
	<div class="content-grid" id="participation">
		<h1 class="overview-title">Participate</h1>
		<br><br><br><br><br><br><br><br><br><br>
		<div class="row">
			<a href="dashboard.php"><div class="btn btn-primary btn-block btn-lg">Return to dashboard</div></a>
		</div>
		<br><br>
	</div>
	
	
	<?php
	}
	?>
	
	
	<?php
	/*
	?>
	
	<div class="content-grid" id="participation">
		<h1 class="overview-title">Participate</h1>
		<br><br><br><br><br><br><br><br><br><br>
		<div class="row">
			<div class="btn btn-primary btn-block btn-lg">Submissions begin on April 1st!</div>
		</div>
		<br><br>
	</div>
	
	<?php
	*/ 
	?>
	
		


		
	</div>
	<!---->
	

	<div class="footer">
		<div class="container">
			
			<p class="footer-class">Copyright © 2015 <a href="http://www.our-stance.org/" target="_blank">STANCE</a> </p>
		</div>
		<script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});

						$(function() {
							  $('a[href*=#]:not([href=#])').click(function() {
							    
							    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
							      var target = $(this.hash);
							      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
							      if (target.length) {
							        $('html,body').animate({
							          scrollTop: target.offset().top
							        }, 1300);
							        return false;
							      }
							    }

							    
							  });
							});
					</script>
					
					<!-- Regarding Ajax form submission  -->
					<script src="js/script.js"></script>
						
			
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	</div>
	<script src="./js/user2.js"></script>
	<!-- Piwik -->
	<script type="text/javascript">
	  var _paq = _paq || [];
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
		var u="//stanceinnovationconclave.com/piwik/";
		_paq.push(['setTrackerUrl', u+'piwik.php']);
		_paq.push(['setSiteId', 1]);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<noscript><p><img src="//stanceinnovationconclave.com/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
	<!-- End Piwik Code -->


</body>
</html>
