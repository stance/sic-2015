<?php

	session_start();


?>
<!--
 .d8888b.  888                                                                    
d88P  Y88b 888                                                                    
Y88b.      888                                                                    
 "Y888b.   888888  8888b.  88888b.   .d8888b .d88b.                               
    "Y88b. 888        "88b 888 "88b d88P"   d8P  Y8b                              
      "888 888    .d888888 888  888 888     88888888                              
Y88b  d88P Y88b.  888  888 888  888 Y88b.   Y8b.                                  
 "Y8888P"   "Y888 "Y888888 888  888  "Y8888P "Y8888                               
                                                                                  
                                                                                  
                                                                                  
8888888                                              888    d8b                   
  888                                                888    Y8P                   
  888                                                888                          
  888   88888b.  88888b.   .d88b.  888  888  8888b.  888888 888  .d88b.  88888b.  
  888   888 "88b 888 "88b d88""88b 888  888     "88b 888    888 d88""88b 888 "88b 
  888   888  888 888  888 888  888 Y88  88P .d888888 888    888 888  888 888  888 
  888   888  888 888  888 Y88..88P  Y8bd8P  888  888 Y88b.  888 Y88..88P 888  888 
8888888 888  888 888  888  "Y88P"    Y88P   "Y888888  "Y888 888  "Y88P"  888  888 
                                                                                  
                                                                                  
                                                                                  
 .d8888b.                             888                                         
d88P  Y88b                            888                                         
888    888                            888                                         
888         .d88b.  88888b.   .d8888b 888  8888b.  888  888  .d88b.               
888        d88""88b 888 "88b d88P"    888     "88b 888  888 d8P  Y8b              
888    888 888  888 888  888 888      888 .d888888 Y88  88P 88888888              
Y88b  d88P Y88..88P 888  888 Y88b.    888 888  888  Y8bd8P  Y8b.                  
 "Y8888P"   "Y88P"  888  888  "Y8888P 888 "Y888888   Y88P    "Y8888               
                                                                                  
                                                                                  
                                                                                  
 .d8888b.   .d8888b.   d888  888888888                                            
d88P  Y88b d88P  Y88b d8888  888                                                  
       888 888    888   888  888                                                  
     .d88P 888    888   888  8888888b.                                            
 .od888P"  888    888   888       "Y88b                                           
d88P"      888    888   888         888                                           
888"       Y88b  d88P   888  Y88b  d88P                                           
888888888   "Y8888P"  8888888 "Y8888P"   
-->
<!DOCTYPE html>
<html>
<head>
<title>Rules || Stance Innovation Conclave 2015 || STANCE</title>
<link href="./css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="./js/jquery.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="./css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="STANCE Innovation Conclave, Innovation Challenge, STANCE, SIC" />
<script type="application/x-javascript">addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Happy+Monkey' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Electrolize:300' rel='stylesheet' type='text/css'>
<link href='css/external-style.css' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!-- slide -->

<!--Start of Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2u9qSbqgRjMPv1KwrsH5ChGkQZ6xrp0x";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Live Chat Script-->

<!-- Start of recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- End of recaptcha -->
</head>
<body >
<!--header-->

	<div class="alert alert-success alert-dismissible reg-form" role="alert">
		<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="color:white">&times;</span><span class="sr-only">Close</span></button>
		<span id="result"></span>
	</div>
	<div class="header hidden-xs hidden-sm">
		<div class="container">	
			<div class="logo">
				<a href="index.php"><img style="margin-top:10px;" src="images/log.png" alt=""></a>
			</div>
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt=""> </span>
					<ul class="nav" >
						<a href="index.php" ><li class="active" >SIC</li></a>
						<?php
						if(isset($_SESSION['authenticated']) AND $_SESSION['authenticated']){
							echo '<a href="dashboard.php" ><li class="active">Dashboard</li></a>';
						}
						?>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

		<div class="header-small hidden-md hidden-lg visible-sm-block visible-xs-block">
		<div class="container">	
			<div class="logo">
				<a href="index.php"><img src="images/log.png" style="margin-top:10px;" alt=""></a>
			</div>
				<div class="top-nav">
					<span class="menu"><img src="images/menu.png" alt=""> </span>
					<ul class="nav" >
						<a href="index.php" ><li class="active" >SIC</li></a>
						<?php
						if(isset($_SESSION['authenticated']) AND $_SESSION['authenticated']){
							echo '<a href="dashboard.php" ><li class="active" >Dashboard</li></a>';
						}
						?>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

<!---->

<br /><br /><br /><br />

	
	<div class="content-grid" id="rules" style='margin-bottom:-30px;margin-top:-10px;'>
		<h1 class="overview-title">SIC 2015 - Details</h1>
		
		<br>
		<div class="row">
			
			<br />
						
						<!-- Modal for prelims' visual rules -->
							<div class="modal fade" id="prelims-visual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel"><b style="class:green">Stance Innovation Conclave 2015</b></h4>
								  </div>
								  <div class="modal-body modal-body-content">
									<ul>
										<li>Register yourself by filling a sign up form <a href="registration_page.php"><b class="text-success">here</b></a>.</li>
										<li>A team can comprise 1-4 members.</li>
										<li>Upload the video showcasing your idea on YouTube, it should be in the time range of 3-8 minutes.</li>
										<li>The title of this video should strictly abide by our naming convention, i.e. 
										<span class="text-success">"Your Innovative Idea Name - Stance Innovation Conclave 2015 (SIC)"</span></li>
										<li>Copy the URL of the video and paste it on your dashboard with title name ending in <b><span class="text-success">"- Stance Innvoation Conclave 2015 (SIC)"</span></b> without double quotes. Once done correctly, the video will be accepted.</li>
										<li><b class="text-failure">Note:</b> The video URL should be strictly in the format <b><span class="text-failure">http://youtube.com/watch?v=XXXXXXXXXX</span></b> and at least with<span class="text-success"> 1 view(s) </span>for your video to be accepted.</li>
										<li>The jury will select the most innovative entries and top 16 teams will be invited to <span class="text-success"> College of Technology, GBPUA&T Pantnagar</span> for their final round.</li>
										<br>
										<li><b>Note:</b>A team can submit any number of entries.</li>
										<li>Every member of the team can sign up separately but the video should be added only by the team leader.</li>
										<br />
									</ul>
								  </div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								  </div>
								</div>
							  </div>
							</div>	

						<!-- modal finishes -->
						
						<!-- Modal for prelims' textual rules -->
							<div class="modal fade" id="prelims-textual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel"><b style="class:green">Stance Innovation Conclave 2015</b></h4>
								  </div>
								  <div class="modal-body modal-body-content">
									<ul>
										<li>Register yourself by filling a sign up form <a href="textual" target="_blank"><b class="text-success">here</b></a>.</li>
										<li>A team can comprise 1-4 members.</li>
										<li><a href="https://docs.google.com/document/d/16MjDyez5U55I-mg09C54erwjBymJM1ZyogMYdsmp6NM/edit?usp=sharing" target="_blank"><span class="text-failure">Click here</span></a> to download the template for textual submission.</li>
										<li>The title of this document should strictly abide by our naming convention, i.e. 
										<span class="text-success">"Your Innovative Idea Name - Stance Innovation Conclave 2015 (SIC).docx"</span></li>
										<li>Once your abstract is ready for submission, you have to upload it at <a href="http://stanceinnovationconclave.com/textual" target="_blank"><b><span class="text-failure">http://stanceinnovationconclave.com/textual</span></b></a>.</li>
										
										<li>The jury will select the most innovative entries and top 16 teams will be invited to <span class="text-success"> College of Technology, GBPUA&T Pantnagar</span> for their final round.</li>
										<br>
										<li><b>Note:</b>A team can submit any number of entries.</li>
										<li>Every member of the team can sign up separately but the video should be added only by the team leader.</li>
										<br />
									</ul>
								  </div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								  </div>
								</div>
							  </div>
							</div>	

						<!-- modal finishes -->
						
						<!-- Modal -->
							<div class="modal fade" id="main-event" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							 <div class="modal-dialog">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel"><b style="class:green">Stance Innovation Conclave 2015</b></h4>
								  </div>
								  <div class="modal-body modal-body-content">
									<ul>
										To be updated soon... 
									</ul>
								  </div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								  </div>
								</div>
							  </div>
							</div>	
						<!-- modal finishes -->
						
						<!-- Modal -->
							<div class="modal fade" id="hackathon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel"><b style="class:green">Stance Innovation Conclave 2015</b></h4>
								  </div>
								  <div class="modal-body modal-body-content">
									<ul>
										To be updated soon... 
									</ul>
								  </div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								  </div>
								</div>
							  </div>
							</div>	
						<!-- modal finishes -->
			
						
						<!-- Modal -->
							<div class="modal fade" id="gbpuat-conclave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel"><b style="class:green">Stance Innovation Conclave 2015</b></h4>
								  </div>
								  <div class="modal-body modal-body-content">
									<ul>
										To be updated soon... 
									</ul>
								  </div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								  </div>
								</div>
							  </div>
							</div>	
						<!-- modal finishes -->
			
						
			<br />
			<h3 class="success-text">Rules & Info (Prelims)</h3>		
			<br />
			<div class="col-md-8 col-md-offset-2" style="text-align:center;">
				
				
				<!-- Modals  for full information -->
				<div class="btn-group" role="group" aria-label="...">	
					<button  type="button" class="btn btn-primary btn-lg more" data-toggle="modal" data-target="#prelims-visual">
					  Visual Method (YouTube Video)
					</button>
					<button  type="button" class="btn btn-primary btn-lg more" data-toggle="modal" data-target="#prelims-textual">
					  Textual Method (Abstract)
					</button>
				</div>
				<!-- Modal for full information -->
				
				
			</div>
			
			<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
		</div>
		
	</div>

	
	
	

		
	</div>
	<!---->
	

	<div class="footer">
		<div class="container">
			
			<p class="footer-class">Copyright © 2015 <a href="http://www.stance.world/" target="_blank">STANCE</a> </p>
		</div>
		
	</div>
	<script src="./js/user2.js"></script>
	<!-- Piwik -->
	<script type="text/javascript">
	  var _paq = _paq || [];
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
		var u="//stanceinnovationconclave.com/piwik/";
		_paq.push(['setTrackerUrl', u+'piwik.php']);
		_paq.push(['setSiteId', 1]);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<noscript><p><img src="//stanceinnovationconclave.com/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
	<!-- End Piwik Code -->


</body>
</html>

