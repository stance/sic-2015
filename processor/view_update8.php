<?php
	require('../handler/database_handler.php');
	
	class view_update extends database_handler{
		private $con;							//Database connection variable
		private $raw_data_from_submissions;		//Raw data of submissions table
		private $url;							//Youtube video URL
		private $id;							//The Youtube ID of the video
		private $vid;							//The Video ID as stored in our database
		private $data_counter;					//The counter which fetches value from our records
		private $views;							//The views on the Youtube video
		
		function __construct(){
			$this->con = parent::connect_to_db();
			$this->update();
		}
		
		private function update(){
			$this->raw_data_from_submissions = $this->fetch_all();
			if(mysqli_num_rows($this->raw_data_from_submissions)){
				$this->execute();
			}
			else{
				$this->reload();
			}
		}
		
		private function fetch_all(){
			$sql = "SELECT * FROM submissions WHERE last_updated=999";
			$result = mysqli_query($this->con, $sql);
			return $result;
		}
		
		private function reload(){
			$sql = "UPDATE submissions SET last_updated=999";
			$result = mysqli_query($this->con, $sql);
		}
		
		//The function responsible for execution of update
		private function execute(){
			
			while($row = $this->raw_data_from_submissions->fetch_assoc()) {    
				//Fetch the first record and then break the while loop    
				$this->vid = $row['video_id'];
				$this->url = $row['url'];
				break;
			}
			$this->id = $this->fetch_id_from_url($this->url);
			$this->views = $this->fetch_views_from_youtube($this->id);
			$sql = "UPDATE submissions SET last_updated=10, views=".$this->views." WHERE video_id=".$this->vid;
			$result = mysqli_query($this->con, $sql);
		}
		
		private function fetch_id_from_url($raw_url){
			$url = htmlspecialchars(trim($raw_url));
			parse_str( parse_url( $url, PHP_URL_QUERY ), $segments );
			$id = $segments['v'];
			return $id;
		}
						
		private function fetch_views_from_youtube($video_ID){
			 //= 'FoiHX9azZeQ';
			$JSON = @file_get_contents("https://gdata.youtube.com/feeds/api/videos/{$video_ID}?v=2&alt=json");
			$JSON_Data = json_decode($JSON);
			$views = $JSON_Data->{'entry'}->{'yt$statistics'}->{'viewCount'};
			return $views;
		}
	}
	
	$update = new view_update;
	
	
?>
